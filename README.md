# Hardware

KiCad version 5.0 or newer is required, 5.1 is recommended.

If you are reading this, you are likely also wondering how to get access to push to this repo.
Currently the repo is read-only, please get in touch directly
via Matrix ( #card10badge:libera.chat ) or IRC ( libera.chat#card10badge, mirror)
if you want to contribute to the hardware.


The project contains two PCBs:

The Fundamental Board:
 - KiCad project file: `boards/Fundamental-Board/Fundamental-Board.pro`


The Harmonic Board:
 - KiCad project file: `boards/Harmonic-Board/Harmonic-Board.pro`


## Known Issues

### 32.768 kHz Crystal

The 32.768 kHz crystal is missing trim capacitors. While the datasheet of the MAX32666 does not
indicate this, they are needed to get the 32 kHz crystal within spec. The errata of the chip
specifies them.

### I2C Port Expander Leakage

The PCAL6408ABS has its ADDR pin connected to VDDI2C. The datasheet specifies that it should be
connected to either VSS or VDDP. According to the datasheet this results in an additional quiescent
current of (max) 80 uA (see page 27 of datasheet) while +3V3 is supplied to the Harmonic Board.
Actual measurements put it more in a region of 100 to 200 uA.

This effectively prevents putting the Harmonic Board into an ultra low power state while still
having access to all buttons. Only the power button which is routed to the PMIC is available in
this state (as +3V3 needs to be switched off).

### UART On USB-C
Diode D502 prevents proper UART communication with the debugger. It was replaced with a 0 Ohm resistor
during production.

### USB Core Supply
VDDB of the MAX32666 is turned on when USB power becomes available. This causes an additional current
draw by the CPU. This heats up the PCB and influences the temperature/humidity measurement of the
BME680 temperature sensor. Making it switchable via firmware would make the card10 a better room climate sensor.

### 0201 Pad Geometry
During production there apparently were issues with the pad geometry of the 0201 devices. Maybe the
solder mask opening is too big, maybe the missing thermals are an issue, maybe the pad size is
wrong in general.

### SD Card
The SD Card is untested.

### Side LED
The Side LED of the Harmonic Board is rotated by 180 deg and shines in the wrong direction.

### Small Add-On Connector
The SOA connector of the Harmonic Board collides with the USB-C connector of the Fundamental Board.

### Wristband
Some GND pads of connectors can come close to the metal parts which secure the wristband. They might
create shorts if the screws of the wristband are tightened too much.

### ECG_VCM
It is unclear if connecting ECG_VCM to the wrist is beneficial or not. Maybe it can
be removed from the wrist connections. It is also available on the USB-C connector.

### PMIC
The chosen OTP version of the MAX77651 only allows up to 100 mA from the USB-C port at cold start. This
limit is reached by the charging capacitances on the 1.8 V and 3.3 V rails. The fundamental board uses
bypass diode D201 to work around this.

### nRST And nEN Lines Missing On USB-C
We did not find a reliable way to connect the nRST line to a free USB 3.0 pin on the USB-C connector.
This can impact debugging via SWD as there is no way to reset the MAX32666 while it is sleeping.

D501, R507, R508 are not populated to achieve this.

### Display Connection
To save some space and allow the battery to be flush with the Harmonic Board, the display is soldered to
the top side of the Harmonic Board. This introduces a potential for large amounts of stress on the glass
substrate when some force is applied to the display. This is the main mechanical weak point of the device.

It might be beneficial to change the layout so the display is soldered to the bottom side of
the Harmonic Board. Together with the double sided tape for the battery this should be safe as well.
