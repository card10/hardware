EESchema Schematic File Version 4
LIBS:Badge_Debugger-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Maxim:MAX32625PICO M1
U 1 1 5D2A2F8A
P 7050 5100
F 0 "M1" H 7600 6100 50  0000 C CNN
F 1 "MAX32625PICO" H 7850 6000 50  0000 C CNN
F 2 "Badge_Debugger:MAX32625PICO" H 7050 4750 50  0001 C CNN
F 3 "" H 7050 4750 50  0001 C CNN
	1    7050 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D2A3BFC
P 9200 1600
F 0 "R1" H 9270 1646 50  0000 L CNN
F 1 "1k" H 9270 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9130 1600 50  0001 C CNN
F 3 "~" H 9200 1600 50  0001 C CNN
	1    9200 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5D2A3E37
P 9200 2000
F 0 "D1" V 9239 1883 50  0000 R CNN
F 1 "LED, green" V 9148 1883 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9200 2000 50  0001 C CNN
F 3 "~" H 9200 2000 50  0001 C CNN
	1    9200 2000
	0    -1   -1   0   
$EndComp
$Comp
L Connector:USB_B_Micro J1
U 1 1 5D2A736B
P 3650 1550
F 0 "J1" H 3420 1447 50  0000 R CNN
F 1 "USB_B_Micro" H 3420 1538 50  0000 R CNN
F 2 "Badge_Debugger:10118192-0001LF" H 3800 1500 50  0001 C CNN
F 3 "~" H 3800 1500 50  0001 C CNN
	1    3650 1550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D2AD8B4
P 9200 2250
F 0 "#PWR0101" H 9200 2000 50  0001 C CNN
F 1 "GND" H 9205 2077 50  0000 C CNN
F 2 "" H 9200 2250 50  0001 C CNN
F 3 "" H 9200 2250 50  0001 C CNN
	1    9200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1750 9200 1850
Wire Wire Line
	9200 2150 9200 2250
$Comp
L Device:R R2
U 1 1 5D2AFA6E
P 9450 1600
F 0 "R2" H 9520 1646 50  0000 L CNN
F 1 "1k" H 9520 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9380 1600 50  0001 C CNN
F 3 "~" H 9450 1600 50  0001 C CNN
	1    9450 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5D2AFA74
P 9450 2000
F 0 "D2" V 9489 1883 50  0000 R CNN
F 1 "LED, green" V 9398 1883 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9450 2000 50  0001 C CNN
F 3 "~" H 9450 2000 50  0001 C CNN
	1    9450 2000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D2AFA7A
P 9450 2250
F 0 "#PWR0102" H 9450 2000 50  0001 C CNN
F 1 "GND" H 9455 2077 50  0000 C CNN
F 2 "" H 9450 2250 50  0001 C CNN
F 3 "" H 9450 2250 50  0001 C CNN
	1    9450 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 1750 9450 1850
Wire Wire Line
	9450 2150 9450 2250
$Comp
L Device:R R3
U 1 1 5D2B0DA0
P 9700 1600
F 0 "R3" H 9770 1646 50  0000 L CNN
F 1 "1k" H 9770 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9630 1600 50  0001 C CNN
F 3 "~" H 9700 1600 50  0001 C CNN
	1    9700 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5D2B0DA6
P 9700 2000
F 0 "D3" V 9739 1883 50  0000 R CNN
F 1 "LED, green" V 9648 1883 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9700 2000 50  0001 C CNN
F 3 "~" H 9700 2000 50  0001 C CNN
	1    9700 2000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D2B0DAC
P 9700 2250
F 0 "#PWR0103" H 9700 2000 50  0001 C CNN
F 1 "GND" H 9705 2077 50  0000 C CNN
F 2 "" H 9700 2250 50  0001 C CNN
F 3 "" H 9700 2250 50  0001 C CNN
	1    9700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1750 9700 1850
Wire Wire Line
	9700 2150 9700 2250
$Comp
L power:+1V8 #PWR0104
U 1 1 5D2B7983
P 9200 1350
F 0 "#PWR0104" H 9200 1200 50  0001 C CNN
F 1 "+1V8" H 9215 1523 50  0000 C CNN
F 2 "" H 9200 1350 50  0001 C CNN
F 3 "" H 9200 1350 50  0001 C CNN
	1    9200 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5D2B80B3
P 9450 1350
F 0 "#PWR0105" H 9450 1200 50  0001 C CNN
F 1 "+3.3V" H 9465 1523 50  0000 C CNN
F 2 "" H 9450 1350 50  0001 C CNN
F 3 "" H 9450 1350 50  0001 C CNN
	1    9450 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5D2B8BE2
P 9700 1350
F 0 "#PWR0106" H 9700 1200 50  0001 C CNN
F 1 "+5V" H 9715 1523 50  0000 C CNN
F 2 "" H 9700 1350 50  0001 C CNN
F 3 "" H 9700 1350 50  0001 C CNN
	1    9700 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1350 9200 1450
Wire Wire Line
	9450 1450 9450 1350
Wire Wire Line
	9700 1350 9700 1450
$Comp
L power:+5V #PWR0107
U 1 1 5D2B9943
P 6850 4100
F 0 "#PWR0107" H 6850 3950 50  0001 C CNN
F 1 "+5V" H 6850 4300 50  0000 C CNN
F 2 "" H 6850 4100 50  0001 C CNN
F 3 "" H 6850 4100 50  0001 C CNN
	1    6850 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR0108
U 1 1 5D2BA2A2
P 7000 4100
F 0 "#PWR0108" H 7000 3950 50  0001 C CNN
F 1 "+1V8" H 7000 4400 50  0000 C CNN
F 2 "" H 7000 4100 50  0001 C CNN
F 3 "" H 7000 4100 50  0001 C CNN
	1    7000 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR0109
U 1 1 5D2BA9EF
P 7100 4100
F 0 "#PWR0109" H 7100 3950 50  0001 C CNN
F 1 "+1V8" H 7115 4273 50  0000 C CNN
F 2 "" H 7100 4100 50  0001 C CNN
F 3 "" H 7100 4100 50  0001 C CNN
	1    7100 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0110
U 1 1 5D2BABE5
P 7250 4100
F 0 "#PWR0110" H 7250 3950 50  0001 C CNN
F 1 "+3.3V" H 7250 4400 50  0000 C CNN
F 2 "" H 7250 4100 50  0001 C CNN
F 3 "" H 7250 4100 50  0001 C CNN
	1    7250 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5D2BC613
P 3450 2650
F 0 "#PWR0111" H 3450 2400 50  0001 C CNN
F 1 "GND" H 3400 2500 50  0000 C CNN
F 2 "" H 3450 2650 50  0001 C CNN
F 3 "" H 3450 2650 50  0001 C CNN
	1    3450 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5D2BCDA2
P 6950 6100
F 0 "#PWR0112" H 6950 5850 50  0001 C CNN
F 1 "GND" H 7050 5950 50  0000 C CNN
F 2 "" H 6950 6100 50  0001 C CNN
F 3 "" H 6950 6100 50  0001 C CNN
	1    6950 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 6000 6850 6100
Wire Wire Line
	6950 6100 6950 6000
Wire Wire Line
	6850 4100 6850 4200
Wire Wire Line
	7000 4200 7000 4100
Wire Wire Line
	7100 4100 7100 4200
Wire Wire Line
	7250 4200 7250 4100
Text GLabel 3000 1450 0    50   Input ~ 0
D-
Text GLabel 3000 1550 0    50   Input ~ 0
D+
Wire Wire Line
	3000 1450 3350 1450
Wire Wire Line
	3350 1550 3000 1550
$Comp
L power:GND #PWR0116
U 1 1 5D2CB4C6
P 3750 1050
F 0 "#PWR0116" H 3750 800 50  0001 C CNN
F 1 "GND" H 3850 900 50  0000 C CNN
F 2 "" H 3750 1050 50  0001 C CNN
F 3 "" H 3750 1050 50  0001 C CNN
	1    3750 1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 1150 3750 1100
Wire Wire Line
	3750 1100 3650 1100
Wire Wire Line
	3650 1100 3650 1150
Connection ~ 3750 1100
Wire Wire Line
	3750 1100 3750 1050
$Comp
L power:+5V #PWR0117
U 1 1 5D2CC875
P 3150 1750
F 0 "#PWR0117" H 3150 1600 50  0001 C CNN
F 1 "+5V" H 3150 1950 50  0000 C CNN
F 2 "" H 3150 1750 50  0001 C CNN
F 3 "" H 3150 1750 50  0001 C CNN
	1    3150 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 1750 3350 1750
$Comp
L Connector:TestPoint TP1
U 1 1 5D2D0EB0
P 2000 1050
F 0 "TP1" V 2058 1168 50  0000 L CNN
F 1 "ECG_P" V 2000 1250 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 2200 1050 50  0001 C CNN
F 3 "~" H 2200 1050 50  0001 C CNN
	1    2000 1050
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2
U 1 1 5D2D3E32
P 9750 4500
F 0 "J2" H 9800 4917 50  0000 C CNN
F 1 "DNP" H 9800 4826 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x06_P2.54mm_Vertical" H 9750 4500 50  0001 C CNN
F 3 "~" H 9750 4500 50  0001 C CNN
	1    9750 4500
	1    0    0    -1  
$EndComp
Text GLabel 9450 4300 0    50   Input ~ 0
P0_0
Text GLabel 9450 4400 0    50   Input ~ 0
P0_2
Text GLabel 10150 4400 2    50   Input ~ 0
P0_3
Text GLabel 9450 4500 0    50   Input ~ 0
P0_4
Text GLabel 10150 4500 2    50   Input ~ 0
P0_5
Text GLabel 9450 4600 0    50   Input ~ 0
P0_6
Text GLabel 10150 4600 2    50   Input ~ 0
P0_7
$Comp
L power:GND #PWR0118
U 1 1 5D2D85EE
P 9450 4800
F 0 "#PWR0118" H 9450 4550 50  0001 C CNN
F 1 "GND" V 9455 4672 50  0000 R CNN
F 2 "" H 9450 4800 50  0001 C CNN
F 3 "" H 9450 4800 50  0001 C CNN
	1    9450 4800
	0    1    1    0   
$EndComp
$Comp
L power:+1V8 #PWR0119
U 1 1 5D2D8989
P 10150 4700
F 0 "#PWR0119" H 10150 4550 50  0001 C CNN
F 1 "+1V8" V 10165 4828 50  0000 L CNN
F 2 "" H 10150 4700 50  0001 C CNN
F 3 "" H 10150 4700 50  0001 C CNN
	1    10150 4700
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0120
U 1 1 5D2D8D65
P 9450 4700
F 0 "#PWR0120" H 9450 4550 50  0001 C CNN
F 1 "+3.3V" V 9465 4828 50  0000 L CNN
F 2 "" H 9450 4700 50  0001 C CNN
F 3 "" H 9450 4700 50  0001 C CNN
	1    9450 4700
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 5D2D902A
P 10150 4800
F 0 "#PWR0121" H 10150 4650 50  0001 C CNN
F 1 "+5V" V 10165 4928 50  0000 L CNN
F 2 "" H 10150 4800 50  0001 C CNN
F 3 "" H 10150 4800 50  0001 C CNN
	1    10150 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 4300 9550 4300
Wire Wire Line
	9550 4400 9450 4400
Wire Wire Line
	9450 4500 9550 4500
Wire Wire Line
	9550 4600 9450 4600
Wire Wire Line
	9450 4700 9550 4700
Wire Wire Line
	9550 4800 9450 4800
Wire Wire Line
	10050 4300 10150 4300
Wire Wire Line
	10150 4400 10050 4400
Wire Wire Line
	10050 4500 10150 4500
Wire Wire Line
	10150 4600 10050 4600
Wire Wire Line
	10050 4700 10150 4700
Wire Wire Line
	10150 4800 10050 4800
Text GLabel 7850 4500 2    50   Input ~ 0
P0_0
Text GLabel 7850 4600 2    50   Input ~ 0
P0_1
Text GLabel 7850 4700 2    50   Input ~ 0
P0_2
Text GLabel 7850 4800 2    50   Input ~ 0
P0_3
Text GLabel 7850 4900 2    50   Input ~ 0
P0_4
Text GLabel 7850 5000 2    50   Input ~ 0
P0_5
Text GLabel 7850 5100 2    50   Input ~ 0
P0_6
Text GLabel 7850 5200 2    50   Input ~ 0
P0_7
Wire Wire Line
	7700 4500 7850 4500
Wire Wire Line
	7850 4600 7700 4600
Wire Wire Line
	7700 4700 7850 4700
Wire Wire Line
	7850 4800 7700 4800
Wire Wire Line
	7700 4900 7850 4900
Wire Wire Line
	7850 5000 7700 5000
Wire Wire Line
	7700 5100 7850 5100
Wire Wire Line
	7850 5200 7700 5200
$Comp
L Memory_Flash:AT25SF081-XMHF-X U1
U 1 1 5D2EC208
P 4750 6550
F 0 "U1" H 5394 6596 50  0000 L CNN
F 1 "DNP" H 5394 6505 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4750 5950 50  0001 C CNN
F 3 "https://www.adestotech.com/wp-content/uploads/DS-AT25SF081_045.pdf" H 4750 6550 50  0001 C CNN
	1    4750 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5D2ECD88
P 4750 7200
F 0 "#PWR0122" H 4750 6950 50  0001 C CNN
F 1 "GND" H 4850 7050 50  0000 C CNN
F 2 "" H 4750 7200 50  0001 C CNN
F 3 "" H 4750 7200 50  0001 C CNN
	1    4750 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0123
U 1 1 5D2ED028
P 4750 5950
F 0 "#PWR0123" H 4750 5800 50  0001 C CNN
F 1 "+3.3V" H 4765 6123 50  0000 C CNN
F 2 "" H 4750 5950 50  0001 C CNN
F 3 "" H 4750 5950 50  0001 C CNN
	1    4750 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5950 4750 6000
Wire Wire Line
	4750 7050 4750 7200
Text Notes 10500 4650 0    50   ~ 0
SPI_CS
Text Notes 10500 4550 0    50   ~ 0
SPI_MOSI
Text Notes 10500 4450 0    50   ~ 0
UART_RTS
Text Notes 10500 4350 0    50   ~ 0
UART_TX
Text Notes 8450 4350 0    50   ~ 0
UART_RX
Text Notes 8450 4450 0    50   ~ 0
UART_CTS / GPIO
Text Notes 8450 4550 0    50   ~ 0
SPI_SCK
Text Notes 8450 4650 0    50   ~ 0
SPI_MISO
Text GLabel 3950 6450 0    50   Input ~ 0
P0_4
Text GLabel 3950 6550 0    50   Input ~ 0
P0_7
Text GLabel 5500 6350 2    50   Input ~ 0
P0_6
Text GLabel 3950 6350 0    50   Input ~ 0
P0_5
$Comp
L Connector:TestPoint TP2
U 1 1 5D2F7078
P 2000 1300
F 0 "TP2" V 2058 1418 50  0000 L CNN
F 1 "ECG_N" V 2000 1500 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 2200 1300 50  0001 C CNN
F 3 "~" H 2200 1300 50  0001 C CNN
	1    2000 1300
	0    -1   -1   0   
$EndComp
Text GLabel 2450 1050 2    50   Input ~ 0
ECG_P
Text GLabel 2450 1300 2    50   Input ~ 0
ECG_N
Wire Wire Line
	2000 1050 2450 1050
Wire Wire Line
	2450 1300 2000 1300
$Comp
L Connector:TestPoint TP3
U 1 1 5D37627C
P 3450 4750
F 0 "TP3" V 3508 4868 50  0000 L CNN
F 1 "ECG_VCM" V 3450 4950 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 3650 4750 50  0001 C CNN
F 3 "~" H 3650 4750 50  0001 C CNN
	1    3450 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5D37ECFF
P 2700 2450
F 0 "TP4" V 2758 2568 50  0000 L CNN
F 1 "PMIC_nEN" V 2700 2650 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 2900 2450 50  0001 C CNN
F 3 "~" H 2900 2450 50  0001 C CNN
	1    2700 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 6450 4150 6450
Wire Wire Line
	4150 6550 3950 6550
Wire Wire Line
	3950 6350 4150 6350
Wire Wire Line
	5350 6350 5500 6350
Wire Wire Line
	4150 6650 4100 6650
Wire Wire Line
	4100 6650 4100 6000
Wire Wire Line
	4100 6000 4750 6000
Connection ~ 4750 6000
Wire Wire Line
	4750 6000 4750 6050
Wire Wire Line
	4150 6750 4100 6750
Wire Wire Line
	4100 6750 4100 6650
Connection ~ 4100 6650
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J3
U 1 1 5D38AD23
P 9750 5400
F 0 "J3" H 9800 5817 50  0000 C CNN
F 1 "DNP" H 9800 5726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x06_P2.54mm_Vertical" H 9750 5400 50  0001 C CNN
F 3 "~" H 9750 5400 50  0001 C CNN
	1    9750 5400
	1    0    0    -1  
$EndComp
Text GLabel 6050 4900 0    50   Input ~ 0
P4_4
Text GLabel 6050 5000 0    50   Input ~ 0
P4_5
Text GLabel 6050 5100 0    50   Input ~ 0
P4_6
Text GLabel 6050 5200 0    50   Input ~ 0
P4_7
Text GLabel 7850 5400 2    50   Input ~ 0
AIN_0
Text GLabel 7850 5500 2    50   Input ~ 0
AIN_1
Text GLabel 9450 5200 0    50   Input ~ 0
P1_6
Text GLabel 10150 5200 2    50   Input ~ 0
P1_7
Text GLabel 9450 5300 0    50   Input ~ 0
P4_4
Text GLabel 10150 5300 2    50   Input ~ 0
P4_5
Text GLabel 9450 5400 0    50   Input ~ 0
P4_6
Text GLabel 10150 5400 2    50   Input ~ 0
P4_7
Text GLabel 9450 5500 0    50   Input ~ 0
AIN_0
Text GLabel 10150 5500 2    50   Input ~ 0
AIN_1
Wire Wire Line
	9450 5200 9550 5200
Wire Wire Line
	9550 5300 9450 5300
Wire Wire Line
	9450 5400 9550 5400
Wire Wire Line
	9550 5500 9450 5500
Wire Wire Line
	10050 5200 10150 5200
Wire Wire Line
	10150 5300 10050 5300
Wire Wire Line
	10050 5400 10150 5400
Wire Wire Line
	10150 5500 10050 5500
Wire Wire Line
	6050 4900 6400 4900
Wire Wire Line
	6400 5000 6050 5000
Wire Wire Line
	6050 5100 6400 5100
Wire Wire Line
	6400 5200 6050 5200
$Comp
L power:+3.3V #PWR0124
U 1 1 5D3BE558
P 9450 5600
F 0 "#PWR0124" H 9450 5450 50  0001 C CNN
F 1 "+3.3V" V 9465 5728 50  0000 L CNN
F 2 "" H 9450 5600 50  0001 C CNN
F 3 "" H 9450 5600 50  0001 C CNN
	1    9450 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5D3BEAB5
P 9450 5700
F 0 "#PWR0125" H 9450 5450 50  0001 C CNN
F 1 "GND" V 9455 5572 50  0000 R CNN
F 2 "" H 9450 5700 50  0001 C CNN
F 3 "" H 9450 5700 50  0001 C CNN
	1    9450 5700
	0    1    1    0   
$EndComp
$Comp
L power:+1V8 #PWR0126
U 1 1 5D3BEF16
P 10150 5600
F 0 "#PWR0126" H 10150 5450 50  0001 C CNN
F 1 "+1V8" V 10165 5728 50  0000 L CNN
F 2 "" H 10150 5600 50  0001 C CNN
F 3 "" H 10150 5600 50  0001 C CNN
	1    10150 5600
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0127
U 1 1 5D3BF8FD
P 10150 5700
F 0 "#PWR0127" H 10150 5550 50  0001 C CNN
F 1 "+5V" V 10165 5828 50  0000 L CNN
F 2 "" H 10150 5700 50  0001 C CNN
F 3 "" H 10150 5700 50  0001 C CNN
	1    10150 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 5600 9550 5600
Wire Wire Line
	9550 5700 9450 5700
Wire Wire Line
	10050 5600 10150 5600
Wire Wire Line
	10150 5700 10050 5700
Wire Wire Line
	7700 5400 7850 5400
Wire Wire Line
	7850 5500 7700 5500
$Comp
L Switch:SW_Push SW1
U 1 1 5D3D6434
P 3100 2650
F 0 "SW1" H 3100 2935 50  0000 C CNN
F 1 "DNP" H 3100 2844 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 3100 2850 50  0001 C CNN
F 3 "~" H 3100 2850 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2650 3300 2650
Wire Wire Line
	6400 4600 6050 4600
Text GLabel 6050 4600 0    50   Input ~ 0
P1_7
Wire Wire Line
	6050 4500 6400 4500
Text GLabel 6050 4500 0    50   Input ~ 0
P1_6
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 5D483416
P 9600 3250
F 0 "J4" H 9650 3667 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 9650 3576 50  0000 C CNN
F 2 "Connector_PinSocket_1.27mm:PinSocket_2x05_P1.27mm_Vertical_SMD" H 9600 3250 50  0001 C CNN
F 3 "~" H 9600 3250 50  0001 C CNN
	1    9600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3250 10100 3250
Wire Wire Line
	10100 3350 9900 3350
Text GLabel 10100 3150 2    50   Input ~ 0
SWC
Text GLabel 10100 3050 2    50   Input ~ 0
SWD
Wire Wire Line
	9900 3050 10100 3050
Wire Wire Line
	10100 3150 9900 3150
$Comp
L power:GND #PWR0128
U 1 1 5D4A7DA1
P 9200 3650
F 0 "#PWR0128" H 9200 3400 50  0001 C CNN
F 1 "GND" H 9300 3500 50  0000 C CNN
F 2 "" H 9200 3650 50  0001 C CNN
F 3 "" H 9200 3650 50  0001 C CNN
	1    9200 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3150 9200 3150
Wire Wire Line
	9200 3150 9200 3250
Wire Wire Line
	9400 3250 9200 3250
Connection ~ 9200 3250
Wire Wire Line
	9200 3250 9200 3650
Text GLabel 10100 3450 2    50   Input ~ 0
Reset
Wire Wire Line
	10100 3450 9900 3450
$Comp
L Switch:SW_Push SW2
U 1 1 5D4B4645
P 4300 3700
F 0 "SW2" H 4300 3985 50  0000 C CNN
F 1 "DNP" H 4300 3894 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 4300 3900 50  0001 C CNN
F 3 "~" H 4300 3900 50  0001 C CNN
	1    4300 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5D4B555D
P 4650 3700
F 0 "#PWR0129" H 4650 3450 50  0001 C CNN
F 1 "GND" H 4600 3550 50  0000 C CNN
F 2 "" H 4650 3700 50  0001 C CNN
F 3 "" H 4650 3700 50  0001 C CNN
	1    4650 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 3700 4500 3700
Text GLabel 4050 3550 1    50   Input ~ 0
Reset
Wire Wire Line
	4050 3550 4050 3700
Wire Wire Line
	4050 3700 4100 3700
Text GLabel 10100 3350 2    50   Input ~ 0
UART_Target_RX
Text GLabel 10100 3250 2    50   Input ~ 0
UART_Target_TX
$Comp
L Regulator_Linear:TLV75518PDBV U3
U 1 1 5D4CEF98
P 6550 1050
F 0 "U3" H 6550 1392 50  0000 C CNN
F 1 "DNP" H 6550 1301 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6550 1350 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv755p.pdf" H 6550 1050 50  0001 C CNN
	1    6550 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5D4E75D2
P 6550 1450
F 0 "#PWR0131" H 6550 1200 50  0001 C CNN
F 1 "GND" H 6555 1277 50  0000 C CNN
F 2 "" H 6550 1450 50  0001 C CNN
F 3 "" H 6550 1450 50  0001 C CNN
	1    6550 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0133
U 1 1 5D4E89BB
P 7450 900
F 0 "#PWR0133" H 7450 750 50  0001 C CNN
F 1 "+3.3V" H 7465 1073 50  0000 C CNN
F 2 "" H 7450 900 50  0001 C CNN
F 3 "" H 7450 900 50  0001 C CNN
	1    7450 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 950  7450 900 
Wire Wire Line
	6550 1450 6550 1350
Wire Wire Line
	6250 1050 6250 950 
$Comp
L power:+5V #PWR0135
U 1 1 5D50EC01
P 6050 950
F 0 "#PWR0135" H 6050 800 50  0001 C CNN
F 1 "+5V" H 6065 1123 50  0000 C CNN
F 2 "" H 6050 950 50  0001 C CNN
F 3 "" H 6050 950 50  0001 C CNN
	1    6050 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 950  6050 950 
Connection ~ 6250 950 
Text Notes 9050 2750 0    50   ~ 0
Target JTAG Connector\nfrom MAX32625PICO -> Debug-Board
$Comp
L Switch:SW_Push SW3
U 1 1 5D2DC13B
P 4300 3000
F 0 "SW3" H 4300 3285 50  0000 C CNN
F 1 "DNP" H 4300 3194 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 4300 3200 50  0001 C CNN
F 3 "~" H 4300 3200 50  0001 C CNN
	1    4300 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0136
U 1 1 5D2DC142
P 4650 3000
F 0 "#PWR0136" H 4650 2750 50  0001 C CNN
F 1 "GND" H 4600 2850 50  0000 C CNN
F 2 "" H 4650 3000 50  0001 C CNN
F 3 "" H 4650 3000 50  0001 C CNN
	1    4650 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 3000 4500 3000
Wire Wire Line
	4050 2850 4050 3000
Wire Wire Line
	4050 3000 4100 3000
Text GLabel 4050 2850 1    50   Input ~ 0
P0_2
$Comp
L Badge_Debugger-rescue:USB3.1_TYPEC-AMP X1
U 1 1 5D416318
P 1950 4350
F 0 "X1" H 1223 4191 50  0000 R CNN
F 1 "USB3.1_TYPEC" H 1223 4100 50  0000 R CNN
F 2 "Badge_Debugger:USB-C-Plug" H 1950 4350 50  0001 C CNN
F 3 "" H 1950 4350 50  0001 C CNN
	1    1950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 5D41F578
P 1650 3250
F 0 "#PWR0113" H 1650 3100 50  0001 C CNN
F 1 "+5V" H 1665 3423 50  0000 C CNN
F 2 "" H 1650 3250 50  0001 C CNN
F 3 "" H 1650 3250 50  0001 C CNN
	1    1650 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 5D41FA0F
P 1750 3250
F 0 "#PWR0114" H 1750 3100 50  0001 C CNN
F 1 "+5V" H 1765 3423 50  0000 C CNN
F 2 "" H 1750 3250 50  0001 C CNN
F 3 "" H 1750 3250 50  0001 C CNN
	1    1750 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0115
U 1 1 5D41FD51
P 1850 3250
F 0 "#PWR0115" H 1850 3100 50  0001 C CNN
F 1 "+5V" H 1865 3423 50  0000 C CNN
F 2 "" H 1850 3250 50  0001 C CNN
F 3 "" H 1850 3250 50  0001 C CNN
	1    1850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3250 1650 3400
Wire Wire Line
	1750 3400 1750 3250
Wire Wire Line
	1850 3250 1850 3400
Text GLabel 2600 4300 2    50   Input ~ 0
D-
Text GLabel 2600 4200 2    50   Input ~ 0
D+
Wire Wire Line
	2450 4100 2450 4200
Wire Wire Line
	2450 4200 2600 4200
Connection ~ 2450 4200
Wire Wire Line
	2450 4400 2450 4300
Wire Wire Line
	2450 4300 2600 4300
Connection ~ 2450 4300
Text GLabel 2600 4650 2    50   Input ~ 0
ECG_N
Text GLabel 2600 4550 2    50   Input ~ 0
ECG_P
Text GLabel 2600 4850 2    50   Input ~ 0
UART_Target_TX
Text GLabel 2600 4750 2    50   Input ~ 0
UART_Target_RX
Wire Wire Line
	2450 4550 2600 4550
Wire Wire Line
	2600 4650 2450 4650
Wire Wire Line
	2450 4750 2600 4750
Wire Wire Line
	2600 4850 2450 4850
Text GLabel 2600 5150 2    50   Input ~ 0
Reset
Text GLabel 2600 3650 2    50   Input ~ 0
SWD
Text GLabel 2600 3750 2    50   Input ~ 0
SWC
Wire Wire Line
	2450 3650 2600 3650
Wire Wire Line
	2600 3750 2450 3750
Wire Wire Line
	2450 5150 2600 5150
$Comp
L power:GND #PWR0137
U 1 1 5D46F5A4
P 1450 5850
F 0 "#PWR0137" H 1450 5600 50  0001 C CNN
F 1 "GND" H 1550 5700 50  0000 C CNN
F 2 "" H 1450 5850 50  0001 C CNN
F 3 "" H 1450 5850 50  0001 C CNN
	1    1450 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5850 1450 5800
Wire Wire Line
	1450 5800 1650 5800
Wire Wire Line
	1650 5800 1650 5700
Connection ~ 1450 5800
Wire Wire Line
	1450 5800 1450 5700
Wire Wire Line
	1650 5800 1750 5800
Wire Wire Line
	1750 5800 1750 5700
Connection ~ 1650 5800
Wire Wire Line
	1750 5800 1850 5800
Wire Wire Line
	1850 5800 1850 5700
Connection ~ 1750 5800
Wire Wire Line
	1850 5800 1950 5800
Wire Wire Line
	1950 5800 1950 5700
Connection ~ 1850 5800
Wire Wire Line
	1950 5800 2100 5800
Wire Wire Line
	2100 5800 2100 5700
Connection ~ 1950 5800
Wire Wire Line
	3450 4950 2450 4950
$Comp
L Device:R R6
U 1 1 5D4FB82F
P 2450 3000
F 0 "R6" H 2520 3046 50  0000 L CNN
F 1 "DNP" H 2520 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2380 3000 50  0001 C CNN
F 3 "~" H 2450 3000 50  0001 C CNN
	1    2450 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5D4FBB4F
P 2950 3000
F 0 "R7" H 3020 3046 50  0000 L CNN
F 1 "DNP" H 3020 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2880 3000 50  0001 C CNN
F 3 "~" H 2950 3000 50  0001 C CNN
	1    2950 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 3000 1950 3400
Text GLabel 2450 2650 0    50   Input ~ 0
P0_3
Wire Wire Line
	3450 4950 3450 4750
Wire Wire Line
	2450 5250 3300 5250
Wire Wire Line
	3300 5250 3300 3000
Wire Wire Line
	3300 3000 3100 3000
Wire Wire Line
	2600 3000 2700 3000
Wire Wire Line
	2450 2650 2700 2650
Wire Wire Line
	2700 2450 2700 2650
Connection ~ 2700 2650
Wire Wire Line
	2700 2650 2900 2650
Wire Wire Line
	2700 2650 2700 3000
Connection ~ 2700 3000
Wire Wire Line
	2700 3000 2800 3000
Wire Wire Line
	6850 950  7450 950 
Wire Wire Line
	2250 3000 2250 3150
Wire Wire Line
	2250 3150 2700 3150
Wire Wire Line
	2700 3150 2700 3000
Wire Wire Line
	1950 3000 2250 3000
Connection ~ 2250 3000
Wire Wire Line
	2250 3000 2300 3000
$Comp
L Graphic:Logo_Open_Hardware_Large #LOGO1
U 1 1 5D315456
P 2500 7250
F 0 "#LOGO1" H 2500 7750 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 2500 6850 50  0001 C CNN
F 2 "Symbol:OSHW-Logo2_7.3x6mm_SilkScreen" H 2500 7250 50  0001 C CNN
F 3 "~" H 2500 7250 50  0001 C CNN
	1    2500 7250
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large #LOGO2
U 1 1 5D3154A0
P 3450 7250
F 0 "#LOGO2" H 3450 7750 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 3450 6850 50  0001 C CNN
F 2 "Symbol:OSHW-Logo2_7.3x6mm_SilkScreen" H 3450 7250 50  0001 C CNN
F 3 "~" H 3450 7250 50  0001 C CNN
	1    3450 7250
	1    0    0    -1  
$EndComp
Text Notes 5000 3000 0    50   ~ 0
Flash
Text Notes 5000 3700 0    50   ~ 0
Reset
Text Notes 3150 2200 0    50   ~ 0
PMIC nEN
Text Notes 5450 6750 0    50   ~ 0
AT25SF081-SSHD-T 
Text Notes 6850 1100 0    50   ~ 0
TLV75533PDBV
$Comp
L Device:Jumper JP2
U 1 1 5D3BDA0D
P 4500 4950
F 0 "JP2" H 4500 5214 50  0000 C CNN
F 1 "Jumper" H 4500 5123 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4500 4950 50  0001 C CNN
F 3 "~" H 4500 4950 50  0001 C CNN
	1    4500 4950
	1    0    0    -1  
$EndComp
Text GLabel 4050 4950 0    50   Input ~ 0
D-
Text GLabel 4050 4600 0    50   Input ~ 0
D+
Wire Wire Line
	4050 4600 4200 4600
Wire Wire Line
	4200 4950 4050 4950
Text GLabel 7850 5650 2    50   Input ~ 0
Debug-D-
Wire Wire Line
	7850 5650 7700 5650
Text GLabel 7850 5750 2    50   Input ~ 0
Debug-D+
Wire Wire Line
	7850 5750 7700 5750
Text GLabel 4950 4950 2    50   Input ~ 0
Debug-D-
Text GLabel 4950 4600 2    50   Input ~ 0
Debug-D+
Wire Wire Line
	4950 4600 4800 4600
Wire Wire Line
	4800 4950 4950 4950
$Comp
L Device:Jumper JP1
U 1 1 5D3B9991
P 4500 4600
F 0 "JP1" H 4500 4864 50  0000 C CNN
F 1 "Jumper" H 4500 4773 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4500 4600 50  0001 C CNN
F 3 "~" H 4500 4600 50  0001 C CNN
	1    4500 4600
	1    0    0    -1  
$EndComp
Text GLabel 6000 2800 0    50   Input ~ 0
SWD
Text GLabel 6000 3150 0    50   Input ~ 0
SWC
Text GLabel 6000 3500 0    50   Input ~ 0
Reset
Text GLabel 6000 2100 0    50   Input ~ 0
UART_Target_RX
Text GLabel 6000 2450 0    50   Input ~ 0
UART_Target_TX
$Comp
L Device:Jumper JP3
U 1 1 5D51F082
P 6350 2100
F 0 "JP3" H 6350 2364 50  0000 C CNN
F 1 "Jumper" H 6350 2273 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6350 2100 50  0001 C CNN
F 3 "~" H 6350 2100 50  0001 C CNN
	1    6350 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP4
U 1 1 5D51F88C
P 6350 2450
F 0 "JP4" H 6350 2714 50  0000 C CNN
F 1 "Jumper" H 6350 2623 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6350 2450 50  0001 C CNN
F 3 "~" H 6350 2450 50  0001 C CNN
	1    6350 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP5
U 1 1 5D51FB65
P 6350 2800
F 0 "JP5" H 6350 3064 50  0000 C CNN
F 1 "Jumper" H 6350 2973 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6350 2800 50  0001 C CNN
F 3 "~" H 6350 2800 50  0001 C CNN
	1    6350 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP6
U 1 1 5D51FE40
P 6350 3150
F 0 "JP6" H 6350 3414 50  0000 C CNN
F 1 "Jumper" H 6350 3323 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6350 3150 50  0001 C CNN
F 3 "~" H 6350 3150 50  0001 C CNN
	1    6350 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP7
U 1 1 5D5202CA
P 6350 3500
F 0 "JP7" H 6350 3764 50  0000 C CNN
F 1 "Jumper" H 6350 3673 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6350 3500 50  0001 C CNN
F 3 "~" H 6350 3500 50  0001 C CNN
	1    6350 3500
	1    0    0    -1  
$EndComp
Text Notes 5150 1050 0    50   ~ 0
TLV75518PDBV
Wire Wire Line
	5150 950  5800 950 
Connection ~ 4550 950 
Wire Wire Line
	4350 950  4550 950 
$Comp
L power:+5V #PWR0134
U 1 1 5D50E5D5
P 4350 950
F 0 "#PWR0134" H 4350 800 50  0001 C CNN
F 1 "+5V" H 4365 1123 50  0000 C CNN
F 2 "" H 4350 950 50  0001 C CNN
F 3 "" H 4350 950 50  0001 C CNN
	1    4350 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1050 4550 950 
Wire Wire Line
	5800 950  5800 900 
Wire Wire Line
	4850 1350 4850 1450
$Comp
L power:+1V8 #PWR0132
U 1 1 5D4E8254
P 5800 900
F 0 "#PWR0132" H 5800 750 50  0001 C CNN
F 1 "+1V8" H 5815 1073 50  0000 C CNN
F 2 "" H 5800 900 50  0001 C CNN
F 3 "" H 5800 900 50  0001 C CNN
	1    5800 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0130
U 1 1 5D4E7080
P 4850 1450
F 0 "#PWR0130" H 4850 1200 50  0001 C CNN
F 1 "GND" H 4855 1277 50  0000 C CNN
F 2 "" H 4850 1450 50  0001 C CNN
F 3 "" H 4850 1450 50  0001 C CNN
	1    4850 1450
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:TLV75518PDBV U2
U 1 1 5D4CD31B
P 4850 1050
F 0 "U2" H 4850 1392 50  0000 C CNN
F 1 "DNP" H 4850 1301 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4850 1350 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv755p.pdf" H 4850 1050 50  0001 C CNN
	1    4850 1050
	1    0    0    -1  
$EndComp
Text GLabel 6700 2100 2    50   Input ~ 0
P0_0
Text GLabel 10150 4300 2    50   Input ~ 0
P0_1
Text GLabel 6700 2450 2    50   Input ~ 0
P0_1
Text GLabel 6700 2800 2    50   Input ~ 0
P0_2
Text GLabel 6700 3150 2    50   Input ~ 0
P0_3
Text GLabel 6700 3500 2    50   Input ~ 0
P1_6
Wire Wire Line
	6000 2100 6050 2100
Wire Wire Line
	6000 2450 6050 2450
Wire Wire Line
	6000 2800 6050 2800
Wire Wire Line
	6000 3150 6050 3150
Wire Wire Line
	6650 2100 6700 2100
Wire Wire Line
	6650 2450 6700 2450
Wire Wire Line
	6650 2800 6700 2800
Wire Wire Line
	6650 3150 6700 3150
Wire Wire Line
	6650 3500 6700 3500
Wire Wire Line
	6050 3500 6000 3500
$Comp
L Device:Jumper JP8
U 1 1 5D721C28
P 8300 2950
F 0 "JP8" H 8300 3214 50  0000 C CNN
F 1 "Jumper" H 8300 3123 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8300 2950 50  0001 C CNN
F 3 "~" H 8300 2950 50  0001 C CNN
	1    8300 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP9
U 1 1 5D7222D2
P 8300 3300
F 0 "JP9" H 8300 3564 50  0000 C CNN
F 1 "Jumper" H 8300 3473 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8300 3300 50  0001 C CNN
F 3 "~" H 8300 3300 50  0001 C CNN
	1    8300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3050 8850 3050
Wire Wire Line
	8850 3050 8850 3300
Wire Wire Line
	8850 3300 8600 3300
Wire Wire Line
	8850 3050 8850 2950
Wire Wire Line
	8850 2950 8600 2950
Connection ~ 8850 3050
$Comp
L power:+3.3V #PWR0138
U 1 1 5D735E86
P 7800 2900
F 0 "#PWR0138" H 7800 2750 50  0001 C CNN
F 1 "+3.3V" H 7800 3200 50  0000 C CNN
F 2 "" H 7800 2900 50  0001 C CNN
F 3 "" H 7800 2900 50  0001 C CNN
	1    7800 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR0139
U 1 1 5D737149
P 7450 2900
F 0 "#PWR0139" H 7450 2750 50  0001 C CNN
F 1 "+1V8" H 7465 3073 50  0000 C CNN
F 2 "" H 7450 2900 50  0001 C CNN
F 3 "" H 7450 2900 50  0001 C CNN
	1    7450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3300 7450 2900
Wire Wire Line
	8000 2950 7800 2950
Wire Wire Line
	7800 2950 7800 2900
Wire Wire Line
	7450 3300 8000 3300
$EndSCHEMATC
