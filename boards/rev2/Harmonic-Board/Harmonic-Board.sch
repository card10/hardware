EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1300 1600 1750 2400
U 5CB104CE
F0 "EKG" 50
F1 "EKG.sch" 50
$EndSheet
$Sheet
S 3650 1600 1800 2400
U 5CB104E6
F0 "Connections" 50
F1 "Connections.sch" 50
$EndSheet
$Sheet
S 5950 1600 1550 2400
U 5D345ECD
F0 "Display Section" 50
F1 "Display.sch" 50
$EndSheet
$Sheet
S 7900 1600 2250 2400
U 5D3957BE
F0 "RGB-LEDs" 50
F1 "LED.sch" 50
$EndSheet
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO1
U 1 1 5CFE33ED
P 10150 5600
F 0 "LOGO1" H 10150 6100 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 10150 5200 50  0001 C CNN
F 2 "Symbol:OSHW-Logo2_7.3x6mm_SilkScreen" H 10150 5600 50  0001 C CNN
F 3 "~" H 10150 5600 50  0001 C CNN
	1    10150 5600
	1    0    0    -1  
$EndComp
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID5
U 1 1 5D1509D0
P 2400 6550
F 0 "FID5" H 2400 6750 50  0000 C CNN
F 1 "Fiducial" H 2400 6675 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2400 6550 50  0001 C CNN
F 3 "~" H 2400 6550 50  0001 C CNN
	1    2400 6550
	1    0    0    -1  
$EndComp
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID4
U 1 1 5D150A38
P 2000 6550
F 0 "FID4" H 2000 6750 50  0000 C CNN
F 1 "Fiducial" H 2000 6675 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2000 6550 50  0001 C CNN
F 3 "~" H 2000 6550 50  0001 C CNN
	1    2000 6550
	1    0    0    -1  
$EndComp
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID3
U 1 1 5D150CC2
P 2800 6150
F 0 "FID3" H 2800 6350 50  0000 C CNN
F 1 "Fiducial" H 2800 6275 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2800 6150 50  0001 C CNN
F 3 "~" H 2800 6150 50  0001 C CNN
	1    2800 6150
	1    0    0    -1  
$EndComp
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID1
U 1 1 5D15140F
P 2000 6150
F 0 "FID1" H 2000 6350 50  0000 C CNN
F 1 "Fiducial" H 2000 6275 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2000 6150 50  0001 C CNN
F 3 "~" H 2000 6150 50  0001 C CNN
	1    2000 6150
	1    0    0    -1  
$EndComp
Text Notes 9850 6100 0    50   ~ 0
Open Hardware
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID6
U 1 1 5D1B1EB8
P 2800 6550
F 0 "FID6" H 2800 6750 50  0000 C CNN
F 1 "Fiducial" H 2800 6675 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2800 6550 50  0001 C CNN
F 3 "~" H 2800 6550 50  0001 C CNN
	1    2800 6550
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO2
U 1 1 5D1B414E
P 9150 5600
F 0 "LOGO2" H 9150 6100 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 9150 5200 50  0001 C CNN
F 2 "badge:artwork-top-2" H 9150 5600 50  0001 C CNN
F 3 "~" H 9150 5600 50  0001 C CNN
	1    9150 5600
	1    0    0    -1  
$EndComp
$Comp
L Harmonic-Board-rescue:Fiducial-Mechanical FID2
U 1 1 5D150F52
P 2400 6150
F 0 "FID2" H 2400 6350 50  0000 C CNN
F 1 "Fiducial" H 2400 6275 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2400 6150 50  0001 C CNN
F 3 "~" H 2400 6150 50  0001 C CNN
	1    2400 6150
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO3
U 1 1 5D1FAE2B
P 7850 5650
F 0 "LOGO3" H 7850 6150 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 7850 5250 50  0001 C CNN
F 2 "badge:artwork-top" H 7850 5650 50  0001 C CNN
F 3 "~" H 7850 5650 50  0001 C CNN
	1    7850 5650
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO4
U 1 1 5D215C47
P 6750 5650
F 0 "LOGO4" H 6750 6150 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 6750 5250 50  0001 C CNN
F 2 "badge:artwork-top-3" H 6750 5650 50  0001 C CNN
F 3 "~" H 6750 5650 50  0001 C CNN
	1    6750 5650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
